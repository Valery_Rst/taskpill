package net.taskPills;

import java.util.Scanner;

public class MainPills {
    //просто шаг
    private static final Time STEP = new Time(0, 15);

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Доброго времени суток! Я помогу Вам составить правильное расписание для приёма таблеток!");
        System.out.print("Укажите время завтрака: ");
        Time breakfast = new Time(scanner.nextLine());

        System.out.print("Укадите время ужина: ");
        Time supper = new Time(scanner.nextLine());

        System.out.print("Укажите интервал между приёмами таблеток: ");
        Time tabletInterval = new Time(scanner.nextLine());

        System.out.print("Укажите за сколько до приёма еды нужно принять таблетку: ");
        Time before = new Time(scanner.nextLine());

        Time available = supper.minus(breakfast); //нам доступно времени (от завтрака до ужина)
        Time lunch = breakfast.plus(available.divide(2)); //обед будет посередине дня

        while(supper.minus(breakfast).isBiggerThan(tabletInterval)) { //пока между ужином и завтраком времени проходит больше, чем нужн
            breakfast = breakfast.plus(STEP); //сближаем завтрак и ужин смещая и то, и то на 15 минут
            supper = supper.minus(STEP);
        }

        System.out.printf("Первый приём таблеток: %s%n", breakfast.minus(before));
        System.out.printf("Завтрак: %s%n", breakfast);
        System.out.printf("Обед: %s%n", lunch);
        System.out.printf("Второй приём таблеток: %s%n", supper.minus(before));
        System.out.printf("Ужин: %s%n", supper);
    }
}